XNAT Container Service Command: edf2xnat
========================================

An XNAT container service command to read EDF headers from resource files and write the value to the XNAT database.

Build
-----

```sh
make build
```

Publish
-------

```sh
make login
make publish
```
